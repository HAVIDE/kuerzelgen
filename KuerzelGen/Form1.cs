﻿using System;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Windows.Forms;

namespace KuerzelGen
{
    //------------------------------------------------------------------------------------------------------------------------------------------------------
    // Program    : KuerzelGen
    // Author     : Robert Elmerhaus
    // Version    : 2.0
    // Description: Generates a shorting of the Name and Surname and checks against the ILOS database if the shorting is available. This program replaces the 
    //              kuerzel_gen_V1.7_LET.vbs script written by Mathias Skaletz
    //------------------------------------------------------------------------------------------------------------------------------------------------------

    public partial class frmMain : Form
    {
        KGenerator kgen;
        string ConnectionString;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------------------------------------------------------------
            // Event   : frmMain_Load()
            // Trigger : While form is loaded
            // Task    : Initialize the form and create needed class
            //-------------------------------------------------------------------------------------------------------------------------------

            txtKuerzel.Focus();
            btnGen.Enabled = false;
            
            ConnectionString = ReadConfig();

            if (ConnectionString.Trim() == "")
                Application.Exit();
        
        } // of frmMain_Load


        private string ReadConfig()
        {
            //-------------------------------------------------------------------------------------------------------------------------------
            // Method     : ReadConfig()
            // Description: Reads the configuration file and returns the connection string
            // Parameters : none
            // Returns    : <string> ................................................................. The connection string
            //-------------------------------------------------------------------------------------------------------------------------------

            string RetVal = "";
            if(File.Exists("kuerzel.cfg"))
            {
                try
                {
                    string[] Content = File.ReadAllLines("kuerzel.cfg");
                    foreach( string Line in Content)
                    {
                        RetVal += Line;
                    }

                    RetVal += ";UID=tsttlb@HDE;PWD=Data3000;";
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Konfigurationsdatei 'kuerzel.cfg' nicht gefunden", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return RetVal;
        } // of ReadConfig();

        private void textName_TextChanged(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------------------------------------------------------------
            // Event   : textName_TextChanged
            // Trigger : Editing Test in Textfield
            // Task    : Enable Generator Button
            //-------------------------------------------------------------------------------------------------------------------------------

            btnGen.Enabled = (textName.Text.Trim() != "");
            txtKuerzel.Text = "";

        } // of textName_TextChanged()

        private void btnExit_Click(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------------------------------------------------------------
            // Event   : btnExit_Click()
            // Trigger : Click on Button "Beenden"
            // Task    : Close the application
            //-------------------------------------------------------------------------------------------------------------------------------

            Application.Exit();

        } // btnExit_Click()

        private void btnGen_Click(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------------------------------------------------------------
            // Event   : btnGen_Click()
            // Trigger : Click on Button "Generieren"
            // Task    : Generate the shorting
            //-------------------------------------------------------------------------------------------------------------------------------

            this.Cursor = Cursors.WaitCursor;

            kgen = new KGenerator(textName.Text);
            kgen.ConnectionString = ConnectionString;
            txtKuerzel.Text = kgen.GenerateShorting();
            this.Cursor = Cursors.Default;

            if (txtKuerzel.Text == "")
                textName.Text = "";

            textName.Focus();

        } // of btnGen_Click()

    } // of frmMain

    class KGenerator
    {
        //-----------------------------------------------------------------------------------------------------------------------------------
        // Class      : KGenerator(<string> FullName)
        // Description: Generates a shorting from a name
        // 
        // Properties : <string> ConnectionString .................................................... ODBC Connection String
        //              <string> Name ................................................................ Full Name
        // 
        // Methods    : <string> GenerateShorting()................................................... Creates the shorting
        //------------------------------------------------------------------------------------------------------------------------------------

        public string ConnectionString { get; set; }
        public string Name { get; internal set; }

        public KGenerator(string FullName)
        {
            //--------------------------------------------------------------------------------------------------------------------------------
            // Constructor : KGenerator(<string> FullName)
            // Description : Initialize the class instance
            // Parameters  : <string> FullName ....................................................... Contains Name and Surname to generate a shorting
            // Returns     : nothing
            //--------------------------------------------------------------------------------------------------------------------------------

            this.Name = FullName;
            this.ConnectionString = "";
        } // of KGenerator

        public string GenerateShorting()
        {
            //--------------------------------------------------------------------------------------------------------------------------------
            // Method    : <string> GenerateShorting()
            // Descrption: Generates a shorting of the Name and Surname and checks against the ILOS database if the shorting is available.
            // Parameters: none
            // Returns   : <string> .................................................................. Generated shorting
            //--------------------------------------------------------------------------------------------------------------------------------

            string RetVal = "";
            string CheckName = _ParseName(this.Name);
            string[] Tokens = CheckName.Split(' ');
            string Vorname = "";
            string Nachname = "";

            if (Tokens.Length == 2)
            {
                Vorname = Tokens[0];
                Nachname = Tokens[1];

                //----------------------------------------------------------------------------------------------------------------------------
                // Try to generate a shorting from 1st letter of Vorname, 1st letter of Nachname and last letter of Nachname
                //----------------------------------------------------------------------------------------------------------------------------
                RetVal = _GenerateFirstFirstLast(Vorname, Nachname);

                if (RetVal != "")
                    return RetVal;

                //----------------------------------------------------------------------------------------------------------------------------
                // Try to generate any 3 char permutation of Vorname and  Nachname
                //----------------------------------------------------------------------------------------------------------------------------

                RetVal = _Permutation( Vorname, Nachname);

                if (RetVal != "")
                    return RetVal;

                MessageBox.Show("Es konnte leider kein Kürzel erzeugt werden. Für diesen Namen sind alle Möglichkeiten ausgeschöpft", "Kürzelgenerator");
            }
            else
            {
                MessageBox.Show("Ein Vor- und Nachname ist für die Kürzelgenerierung erforderlich","Kürzelgenerator");
            }

            return RetVal;
        } // of GetShorting()

        private string _GenerateFirstFirstLast(string vorName,string nachName)
        {
            //--------------------------------------------------------------------------------------------------------------------------------
            // Method    : <string> _GenerateFirstFirstLast(<string> vorName,<string> nachName)
            // Descrption: Try to generate a shorting from 1st letter of Vorname, 1st letter of Nachname and last letter of Nachname
            // Parameters: <string> vorName .......................................................... Given name
            //             <string> nachName ......................................................... Surname
            // Returns   : <string> .................................................................. Generated shorting
            //--------------------------------------------------------------------------------------------------------------------------------

            string RetVal = "";
            string Check = vorName.Substring(0, 1) + nachName.Substring(0, 1) + nachName.Substring(nachName.Length - 1, 1);

            if (!_ShortingExists(Check))
                RetVal = Check;

            return RetVal;
        } // of _GenerateFirstFirstLast()

        private string _Permutation(string Vorname, string Nachname)
        {
            //--------------------------------------------------------------------------------------------------------------------------------
            // Method    : <string> _Permutation(<string> Vorname,<string> Nachname)
            // Descrption: Determine any 3 character permutations of Vorname and Nachname
            // Parameters: <string> Vorname .......................................................... Given name
            //             <string> Nachname ......................................................... Surname
            // Returns   : <string> .................................................................. Generated Permutation
            //--------------------------------------------------------------------------------------------------------------------------------

            string RetVal = "";
            string FullString = Vorname + Nachname;
            string Check = "";

            for (int i = 0; i < FullString.Length - 2; i++)
            {
                for (int j = i + 1; j < FullString.Length; j++)
                {
                    for (int k = j + 1; k < FullString.Length; k++)
                    {
                        Check = FullString.Substring(i, 1) + FullString.Substring(j, 1) + FullString.Substring(k, 1);

                        if (!_ShortingExists(Check))
                            RetVal = Check;

                        if (RetVal != "")
                            k = FullString.Length;

                    } // of for(k)

                    if (RetVal != "")
                        j = FullString.Length;
                } // of for(j)

                if (RetVal != "")
                    i = FullString.Length;

            } // of for(i)

            return RetVal;
        } // of _Permutation()

        private bool _ShortingExists(string Shorting)
        {
            //--------------------------------------------------------------------------------------------------------------------------------
            // Method    : <bool> _ShortingExists(<string> Shorting)
            // Descrption: Check, if a shorting 
            // Parameters: <string> Shorting ......................................................... Shorting string to check
            // Returns   : <bool> .................................................................... True if Shorting exists already
            //--------------------------------------------------------------------------------------------------------------------------------

            bool RetVal = false;
            OdbcConnection connection = new OdbcConnection(this.ConnectionString);
            OdbcCommand cmd = new OdbcCommand("SELECT \"empl-code\",\"empl-nm\"  FROM PUB.empl WHERE \"empl-code\" = '" + Shorting + "'", connection);
            OdbcDataAdapter da = new OdbcDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            if (ds.Tables[0].Rows.Count != 0)
                RetVal = true;

            connection.Close();
            return RetVal;
        } // of ShortingExists()


        private string _ParseName(string Name)
        {
            //--------------------------------------------------------------------------------------------------------------------------------
            // Method    : <string> _ParseName(<string> Name)
            // Descrption: Prepares the name to calculate the shorting
            // Parameters: <string> Name ............................................................. User entered name
            // Returns   : <string> .................................................................. Generated shorting
            //--------------------------------------------------------------------------------------------------------------------------------

            string RetVal = "";

            //--------------------------------------------------------------------------------------------------------------------------------
            // Step 1: Reduce spaces to only 1 space
            //--------------------------------------------------------------------------------------------------------------------------------

            string[] Tokens = Name.Split(' ');
            string WorkingString = "";
            // The name contains more than 2 words?
            WorkingString = Tokens[0];

            if (Tokens.Length > 2)
            {
                for(int i =1;i < Tokens.Length -1;i++)
                {
                    WorkingString += Tokens[i];
                }

            }

            if(Tokens.Length != 1)
                WorkingString += " " + Tokens[Tokens.Length - 1];

            //--------------------------------------------------------------------------------------------------------------------------------
            // Step 2: Convert to uppecase and replace umlauts
            //--------------------------------------------------------------------------------------------------------------------------------

            WorkingString = WorkingString.ToUpper();
            WorkingString = WorkingString.Replace("Ä", "AE");
            WorkingString = WorkingString.Replace("Ö", "OE");
            WorkingString = WorkingString.Replace("Ü", "UE");

            //--------------------------------------------------------------------------------------------------------------------------------
            // Step 3: Remove special Characters
            //--------------------------------------------------------------------------------------------------------------------------------

            for(int i =0;i< WorkingString.Length;i++)
            {
                int Ascii = (int)WorkingString[i];
                if ((Ascii >= 65 && Ascii <= 90) || Ascii == 32)
                    RetVal += WorkingString[i];
            }

            return RetVal;
        }
    } // of class KGenerator
} // of KuerzelGen
